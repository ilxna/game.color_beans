/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/30
 */
game.PassEffect = cc.Class.extend({
    _node : null,
    _action : null,
    _layer : null,
    ctor:function(delayTime, actionTime){
        var self = this;
        var winSize = cc.winSize;
        var index = Math.random() > 0.5 ? 0 : 1;
        this._node = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('star' + index + '.png'));
        this._node.attr({
            x : Math.random() * winSize.width,
            y : Math.random() * winSize.height,
            scale : 0,
            rotation : Math.random() * 360 * (Math.random() > 0.5 ? -1 : 1),
        });

        var rotation = Math.random() * 360 * (Math.random() > 0.5 ? -1 : 1);
        var scale = Math.random() * 2;
        this._action = new cc.Sequence(
            new cc.DelayTime(delayTime),
            new cc.Spawn(
                new cc.ScaleTo(actionTime * 0.4, scale),
                new cc.RotateBy(actionTime * 0.4, rotation)
            ),
            new cc.FadeOut(actionTime * 0.6),
            new cc.CallFunc(function(){
                self.removeFromLayer();
            })
        );
    },

    addToLayer : function(layer){
        this._layer = layer;
        layer.addChild(this._node);
        this._node.runAction(this._action);
    },

    removeFromLayer : function(){
        this._node.removeFromParent();
    },
});