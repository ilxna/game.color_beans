/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/29
 */
game.TipsEffect = cc.Class.extend({
    _node : null,
    _layer : null,
    _action : null,
    ctor:function(pos){
        var self = this;
        var winSize = cc.winSize;

        this._node = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('ui/haha.png'));
        this._node.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : winSize.width / 2,
            y : winSize.height / 2,
            scale : 5,
            opacity : 1,
        });

        this._action = new cc.Sequence(
            new cc.Spawn(
                new cc.MoveTo(1, pos),
                new cc.FadeTo(1, 255),
                new cc.ScaleTo(1, 1)
            ),
            new cc.DelayTime(1),
            new cc.CallFunc(function(){
                self.removeFromLayer();
            })
        );

    },

    addToLayer : function(layer){
        this._layer = layer;
        var self = this;
        layer.addChild(this._node);
        this._node.runAction(this._action);

    },

    removeFromLayer : function(){
        this._node.removeFromParent();
    },




});