/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/29
 */
game.SubTimeEffect = cc.Class.extend({
    _lbl : null,
    _layer : null,
    _action : null,
    ctor:function(pos){
        var self = this;
        var winSize = cc.winSize;

        this._lbl = new cc.LabelTTF('时间\n  -4', 'Arial', 14);
        this._lbl.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : pos.x,
            y : pos.y,
            color : cc.color(0, 234, 255),
            hAlignment : cc.TEXT_ALIGNMENT_CENTER,
            vAlignment : cc.VERTICAL_TEXT_ALIGNMENT_TOP,
        });

        this._action = new cc.Sequence(new cc.Spawn(
            new cc.MoveBy(1, cc.p(
                0, 20
            )),
            new cc.FadeTo(1, 0)
        ), new cc.CallFunc(function(){
            self.removeFromLayer();
        }));

    },

    addToLayer : function(layer){
        this._layer = layer;
        var self = this;
        layer.addChild(this._lbl);
        this._lbl.runAction(this._action);

    },

    removeFromLayer : function(){
        this._lbl.removeFromParent();
    },




});