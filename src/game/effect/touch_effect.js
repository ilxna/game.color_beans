/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/30
 */
game.TouchEffect = cc.Class.extend({
    _node : null,
    _layer : null,
    _action : null,
    ctor:function(pos){
        var self = this;
        var winSize = cc.winSize;

        this._node = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('ui/haha.png'));
        this._node.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : pos.x,
            y : pos.y,
        });

        this._action = new cc.Sequence(

            new cc.FadeIn(0.1),
            new cc.FadeOut(0.5),

            new cc.CallFunc(function(){
                self.removeFromLayer();
            })
        );

    },

    addToLayer : function(layer){
        this._layer = layer;
        var self = this;
        layer.addChild(this._node);
        this._node.runAction(this._action);

    },

    removeFromLayer : function(){
        this._node.removeFromParent();
    },




});