/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/26
 */
game.ColorBean = game.BaseRole.extend({
    _layer : null,
    _colorType : null,

    _levelPos : null,
    ctor:function(beanType, colorTypes){
        this._super();

        var type = Math.floor(Math.random() * colorTypes);

        this._colorType = type;

        this._node = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame(beanType + '/'+type+'.png'));
    },

    addToLayer : function(layer){
        this._layer = layer;
        layer.addChild(this._node);
    },

    removeFromLayer : function(touchPos){
        var self = this;

        if(touchPos){

            //cc.log('touch pos...', touchPos);

            if(touchPos.x == this._levelPos.x){
                var y = this._levelPos.y;
                var step = this._levelPos.y > touchPos.y  ? -1 : 1;

                while(y != touchPos.y){

                    var p = cc.p(
                        (touchPos.x + 0.5) * this._layer._rect_width,
                        (y + 0.5) * this._layer._rect_width
                    );

                    //cc.log('p==', p);

                    (new game.RemoveBeansEffect(this._layer._bean_type, this._colorType, p)).addToLayer(this._layer);


                    y += step;
                }
            }else if(touchPos.y == this._levelPos.y){
                var x = this._levelPos.x;
                var step = this._levelPos.x > touchPos.x  ? -1 : 1;

                while(x != touchPos.x){
                    var p = cc.p(
                        (x + 0.5) * this._layer._rect_width,
                        (touchPos.y + 0.5) * this._layer._rect_width
                    );

                    //cc.log('p==', p);
                    (new game.RemoveBeansEffect(this._layer._bean_type, this._colorType, p)).addToLayer(this._layer);

                    x += step;
                }
            }


            var x =  this._layer._rect_width * ([-1, 0, 1].getRandomItem());
            var y = -(this._levelPos.y * this._layer._rect_width + this._layer._rect_width * 0.5) - 10;
            var speed = -300;
            var time = y / speed;
            this._node.runAction(new cc.Sequence(new cc.MoveBy(time, cc.p(x, y)), new cc.CallFunc(function(){
                self._node.removeFromParent();
            })));

        }else{
            self._node.removeFromParent();
        }






    },

    setLevelPos:function(x, y){

        this._levelPos = cc.p(x, y);

    },


});