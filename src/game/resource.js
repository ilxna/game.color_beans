



var loadingRes = {
    loading_png : "res/" + game._Config.language + "/loading.png"
};

var loaderRes = {
    //loader_plist : "res/" + game._Config.language + "/plist/loader.plist",
    //loader_png : "res/" + game._Config.language + "/plist/loader.png",
    //loader_bg_jpg : "res/" + game._Config.language + "/jpg/loader_bg.jpg",
    //loading_bar_png : "res/common/loading_bar.png",
    loading2_png : "res/" + game._Config.language + "/loading2.png",
};

var res = {
    menu_bg_jpg : "res/common/jpg/menu_bg.jpg",
    bg_jpg : "res/common/jpg/bg.jpg",

    logo_png : "res/" + game._Config.language + "/logo.png",
    start_png : "res/" + game._Config.language + "/start.png",


    guide1_png : "res/" + game._Config.language + "/guide1.png",

    gamelayer_plist : "res/common/gamelayer.plist",
    gamelayer_png : "res/common/gamelayer.png",

    pause_png : "res/" + game._Config.language + "/pause.png",

    over_png : "res/" + game._Config.language + "/over.png",
    pass_png : "res/" + game._Config.language + "/pass.png",


};


var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}

//cc.log(g_resources);


var g_loaderResources = [];
for (var i in loaderRes) {
    g_loaderResources.push(loaderRes[i]);
}
