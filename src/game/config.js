/**
 * Created by zhaojm on 15/3/17.
 */
game._Config = {
    language : game._Enum.language.en,    // cn or en
    //show_ads : false,
    debug : true,


    title : {
        en : 'color beans',
        cn : '十字消彩豆',
    },

};

game._Config.getLevelData = function(level){
    cc.assert(level >= 0 && level < 10, 'level error');


    var data = {
        bean_count : 50,    // 数量
        color_type : 5,     // 颜色种类
        pass_limit : 2, // <= 2 // 过关剩余个数
        time_limit : 40,    // 时间限制
    };

    data.bean_count += level * 5;
    data.color_type += level * 1;
    data.pass_limit += level * 1;
    data.time_limit += level * 5;

    return data;

};