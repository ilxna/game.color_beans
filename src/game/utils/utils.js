/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/18
 */
game._Utils = {};

game._Utils.goToHomePage = function(){
    window.location.href="http://mingz.me";
};

game._Utils.setTitle = function(name){
    document.title = name;
};

game._Utils.getFrameSize = function(){
    var clientWidth = document.body.clientWidth;
    var clientHeight = document.body.clientHeight;

    return cc.size(clientWidth, clientHeight);

};

