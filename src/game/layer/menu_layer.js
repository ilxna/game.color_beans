/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/14
 */
game.MenuLayer = cc.Layer.extend({
    ctor:function(){
        this._super();
        var winSize = cc.winSize;

        var bg = new cc.Sprite(res.menu_bg_jpg);
        bg.attr({
            anchorX : 0.5,
            anchorY : 0,
            x : winSize.width / 2,
            y : 0,
        });
        this.addChild(bg);


        var logo = new cc.Sprite(res.logo_png);
        logo.attr({
            x : winSize.width / 2,
            y : 640 * 0.55,
        });
        this.addChild(logo);




        var self = this;

        var startItem = new cc.MenuItemImage(res.start_png, res.start_png, res.start_png, function(){
            cc.director.runScene(new game.GameScene());
        }, this);
        startItem.attr({
            x : winSize.width / 2,
            y : 640 * 0.2,
        });



        var menu = new cc.Menu(startItem);
        //menu.alignItemsVertically();
        menu.setPosition( cc.p(0, 0));
        //menu.setContentSize(bgSize);
        this.addChild(menu);

    },
});