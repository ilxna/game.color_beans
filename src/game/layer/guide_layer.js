/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/27
 */
game.GuideLayer = cc.Layer.extend({
    ctor:function(callback){
        this._super();
        var winSize = cc.winSize;
        var self = this;

        var g1 = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('guide0.png'));
        g1.attr({
            anchorX : 0.5,
            anchorY : 0,
            x : winSize.width / 2,
            y : winSize.height / 2,
        });
        this.addChild(g1);

        var g2 = new cc.Sprite(res.guide1_png);
        g2.attr({
            anchorX : 0.5,
            anchorY : 1,
            x : winSize.width / 2,
            y : winSize.height / 2 - 10,
        });
        this.addChild(g2);

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true   ,       // true 为不向下传递
            onTouchBegan: function(touch, event){
                return true;
            },
            onTouchEnded:function(touch, event){
                self.removeFromLayer(function(){
                    callback();
                });
            },
        }, this);


    },

    removeFromLayer : function(callback){
        this.removeFromParent();
        callback();
    },

});