/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/28
 */
game.PassLayer = cc.Layer.extend({
    _node : null,
    ctor:function(level, score, timer, data){
        this._super();
        var self = this;

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true   ,       // true 为不向下传递
            onTouchBegan: function(touch, event){
                return true;
            },
        }, this);


        this.effect(function(){
            self.createNode(level, score, timer, data);
        });
    },


    effect : function(callback){
        var allTime = 8;
        this.runAction(new cc.Sequence(new cc.DelayTime(allTime), new cc.CallFunc(
            callback
        )));

        var maxDelayTime = 4;
        var maxActionTime = allTime - maxDelayTime;

        for(var i = 0; i < 30; i++){
            var delayTime = Math.random() * maxDelayTime;
            var actionTime = maxActionTime;
            (new game.PassEffect(delayTime, actionTime)).addToLayer(this);
        }


    },



    createNode:function(level, score, timer, data){
        var self = this;
        var winSize = cc.winSize;


        var node = new cc.Node();
        this._node = node;


        var bg = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('levelover/bg.png'));
        bg.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : bg.width / 2,
            y : bg.height / 2,
        });
        node.addChild(bg);

        node.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            width : bg.width,
            height : bg.height,
            x : winSize.width / 2,
            y : winSize.height / 2,
        });

        var passtxt = new cc.Sprite(res.pass_png);
        passtxt.attr({
            anchorY : 1,
            anchorX : 0.5,
            x : bg.width / 2,
            y : bg.height - 10,
        });
        node.addChild(passtxt);

        var pass = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('levelover/pass.png'));
        pass.attr({
            anchorX : 0.5,
            anchorY : 1,
            x : bg.width / 2,
            y : passtxt.y - passtxt.height - 10,
        });
        node.addChild(pass);



        var levelLbl = new cc.LabelTTF('完成关卡 : ' + level, 'Arial', 18);
        levelLbl.attr({
            anchorX : 0,
            anchorY : 1,
            x : 10,
            y : pass.y - pass.height - 10,
            color : cc.color(255, 255, 255),
        });
        this._node.addChild(levelLbl);

        var timerLbl = new cc.LabelTTF('关卡用时 : '+timer+'秒', 'Arial', 18);
        timerLbl.attr({
            anchorX : 1,
            anchorY : 1,
            x : bg.width - 10,
            y : pass.y - pass.height - 20,
            color : cc.color(255, 255, 255),
        });
        this._node.addChild(timerLbl);

        var bestLbl = new cc.LabelTTF('本关最好记录 : '+timer+'秒', 'Arial', 18);
        bestLbl.attr({
            anchorX : 0.5,
            anchorY : 1,
            x : bg.width / 2,
            y : timerLbl.y - timerLbl.height - 10,
            color : cc.color(255, 255, 255),
        });
        this._node.addChild(bestLbl);

        var totalLbl = new cc.LabelTTF('总得分 : ' + score, 'Arial', 18);
        totalLbl.attr({
            anchorX : 0.5,
            anchorY : 1,
            x : bg.width / 2,
            y : bestLbl.y - bestLbl.height - 10,
            color : cc.color(255, 255, 0),
        });
        this._node.addChild(totalLbl);



        var homeFrame  = cc.spriteFrameCache.getSpriteFrame('ui/home.png');
        var homeItem = new cc.MenuItemImage(homeFrame, homeFrame, homeFrame, function(){
            window.location.href="http://mingz.me";
        }, this);

        var startFrame  = cc.spriteFrameCache.getSpriteFrame('ui/start.png');
        var startItem = new cc.MenuItemImage(startFrame, startFrame, startFrame, function(){
            self.removeFromLayer();
            //if(game._Config.show_ads && game._Config.language == game._Enum.language.en) {
            //    window['Ads']['topAds']();
            //}
            self.parent.newLevel();
        }, this);

        var restartFrame  = cc.spriteFrameCache.getSpriteFrame('ui/restart.png');
        var restartItem = new cc.MenuItemImage(restartFrame, restartFrame, restartFrame, function(){
            self.removeFromLayer();
            self.parent.levelAgain();

        }, this);

        var menu = new cc.Menu(homeItem, restartItem, startItem);
        menu.alignItemsHorizontally();
        menu.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : bg.width / 2,
            y : bg.height * 0.1,
        });
        node.addChild(menu);


        this.addChild(node);
    },


    removeFromLayer : function(){
        var self = this;
        var winSize = cc.winSize;
        this._node.runAction(new cc.Sequence(
            new cc.MoveTo(0.4, cc.p(winSize.width / 2, -10)).easing(cc.easeBackIn()),
            new cc.CallFunc(function(){
                self.removeFromParent();
            })
        ));
    },





});