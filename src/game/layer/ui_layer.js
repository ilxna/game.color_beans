/**
 * Created by zhaojm on 15/4/14.
 */
game.UILayer = cc.Layer.extend({

    _scoreLbl : null,
    _levelLbl : null,
    _pass_limit_lbl : null,
    _timerLbl : null,
    ctor:function(){
        this._super();
        var winSize = cc.winSize;
        var self = this;

        this._levelLbl = new cc.LabelTTF('第1关', 'Arial', 20);
        this._levelLbl.attr({
            anchorX : 0,
            anchorY : 1,
            x : 10,
            y : winSize.height - 10,
            color : cc.color(85, 12, 233),
            scale : 0.5,
        });
        this.addChild(this._levelLbl);

        this._scoreLbl = new cc.LabelTTF('得分:0', 'Arial', 20);
        this._scoreLbl.attr({
            anchorX : 0,
            anchorY : 1,
            x : 10,
            y : winSize.height - 35,
            color : cc.color(85, 12, 233),
            scale : 0.5,
        });
        this.addChild(this._scoreLbl);

        this._pass_limit_lbl = new cc.LabelTTF('剩余图案<=5(110)', 'Arial', 20);
        this._pass_limit_lbl.attr({
            anchorX : 0,
            anchorY : 1,
            x : 10,
            y : winSize.height - 60,
            color : cc.color(85, 12, 233),
            scale : 0.5,
        });
        this.addChild(this._pass_limit_lbl);

        var timer = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('ui/timer.png'));
        timer.attr({
            x : 80,
            y : winSize.height - 10,
            anchorY : 1,
            anchorX : 0.5,
            //scale : 0.5,
        });
        this.addChild(timer);

        this._timerLbl = new cc.LabelTTF('0.21', 'Arial', 16);
        this._timerLbl.attr({
            anchorX : 0.5,
            anchorY : 1,
            x : 80,
            y : winSize.height - 45,
            color : cc.color(85, 12, 233),
            scale : 0.5,
        });
        this.addChild(this._timerLbl);




        var pauseFrame = cc.spriteFrameCache.getSpriteFrame('ui/pause.png');
        var pauseItem = new cc.MenuItemImage(pauseFrame, pauseFrame, pauseFrame, function(){
            self.addChild(new game.PauseLayer(), 2);
        }, this);
        //pauseItem.attr({
        //    anchorY : 1,
        //    x : winSize.width / 2,
        //    y : winSize.height - 10,
        //});

        var bombFrame = cc.spriteFrameCache.getSpriteFrame('ui/bomb.png');
        var bombItem = new cc.MenuItemImage(bombFrame, bombFrame, bombFrame, function(){

            if(self.parent._status != game._Enum.status_enum.normal){
                return;
            }

            self.parent._status = game._Enum.status_enum.bomb;
            self.parent.needBomb();

            bombItem.setVisible(false);
            self.scheduleOnce(function(){
                bombItem.setVisible(true);
            }, 5);

        }, this);


        var resumeFrame = cc.spriteFrameCache.getSpriteFrame('ui/resume.png');
        var resumeItem = new cc.MenuItemImage(resumeFrame, resumeFrame, resumeFrame, function(){
            self.parent.resumeBeans();

            resumeItem.setVisible(false);
            self.scheduleOnce(function(){
                resumeItem.setVisible(true);
            }, 5);

        }, this);


        var croseFrame = cc.spriteFrameCache.getSpriteFrame('ui/crose.png');
        var croseItem = new cc.MenuItemImage(croseFrame, croseFrame, croseFrame, function(){

            if(self.parent._status != game._Enum.status_enum.normal){
                return;
            }

            self.parent._status = game._Enum.status_enum.crose;
            self.parent.needCrose();

            croseItem.setVisible(false);
            self.scheduleOnce(function(){
                croseItem.setVisible(true);
            }, 5);

        }, this);

        var tipsFrame = cc.spriteFrameCache.getSpriteFrame('ui/tips.png');
        var tipsItem = new cc.MenuItemImage(tipsFrame, tipsFrame, tipsFrame, function(){
            self.parent.tips();

            tipsItem.setVisible(false);
            self.scheduleOnce(function(){
                tipsItem.setVisible(true);
            }, 5);

        }, this);


        var menu = new cc.Menu(pauseItem, bombItem, resumeItem, croseItem, tipsItem);
        menu.alignItemsHorizontally();
        menu.attr({
            anchorY : 1,
            //anchorX : 1,
            x : winSize.width / 2 + 50,
            y : winSize.height - 40,
            //width : winSize.width,
            //height : tipsItem.height,

        });
        this.addChild(menu);


    },

    refresh : function(level, score, countTimer, data){
        this.setPassLimit(data.pass_limit, data.bean_count);
        this.setLevel(level);
        this.setScore(score);
        this.setTimer(data.time_limit - countTimer);
    },


    setLevel : function(level){
        this._levelLbl.setString('第'+level+'关');
    },

    setPassLimit : function(limit, current){
        this._pass_limit_lbl.setString('剩余图案<=' + limit + '(' + current + ')');
    },


    setScore : function(score){
        this._scoreLbl.setString('得分:' + score);
    },

    setTimer : function(seconds){
        var minute = Math.floor(seconds / 60);
        var second = seconds % 60;
        this._timerLbl.setString(minute + ':' + second);
    },








});