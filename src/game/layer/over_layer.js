/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/28
 */
game.OverLayer = cc.Layer.extend({
    _node : null,
    ctor:function(level, data){
        this._super();
        var self = this;
        var winSize = cc.winSize;

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true   ,       // true 为不向下传递
            onTouchBegan: function(touch, event){
                return true;
            },
        }, this);


        var node = new cc.Node();
        this._node = node;


        var bg = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('levelover/bg.png'));
        bg.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : bg.width / 2,
            y : bg.height / 2,
        });
        node.addChild(bg);

        node.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            width : bg.width,
            height : bg.height,
            x : winSize.width / 2,
            y : winSize.height / 2,
        });

        var passtxt = new cc.Sprite(res.over_png);
        passtxt.attr({
            anchorY : 1,
            anchorX : 0.5,
            x : bg.width / 2,
            y : bg.height - 20,
        });
        node.addChild(passtxt);

        var pass = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('levelover/over.png'));
        pass.attr({
            anchorX : 0.5,
            anchorY : 1,
            x : bg.width / 2,
            y : passtxt.y - passtxt.height - 30,
        });
        node.addChild(pass);



        var levelLbl = new cc.LabelTTF('失败关卡 : ' + level, 'Arial', 18);
        levelLbl.attr({
            anchorX : 0.5,
            anchorY : 1,
            x : bg.width / 2,
            y : pass.y - pass.height - 20,
            color : cc.color(255, 255, 255),
        });
        this._node.addChild(levelLbl);



        var homeFrame  = cc.spriteFrameCache.getSpriteFrame('ui/home.png');
        var homeItem = new cc.MenuItemImage(homeFrame, homeFrame, homeFrame, function(){
            window.location.href="http://mingz.me";
        }, this);

        var freeFrame  = cc.spriteFrameCache.getSpriteFrame('ui/free.png');
        var freeItem = new cc.MenuItemImage(freeFrame, freeFrame, freeFrame, function(){
            //self.removeFromLayer();
            //if(game._Config.show_ads && game._Config.language == game._Enum.language.en) {
            //    window['Ads']['topAds']();
            //}
        }, this);

        var restartFrame  = cc.spriteFrameCache.getSpriteFrame('ui/restart.png');
        var restartItem = new cc.MenuItemImage(restartFrame, restartFrame, restartFrame, function(){
            self.removeFromLayer();
            self.parent.levelAgain();
        }, this);

        var menu = new cc.Menu(homeItem, restartItem, freeItem);
        menu.alignItemsHorizontally();
        menu.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : bg.width / 2,
            y : bg.height * 0.15,
        });
        node.addChild(menu);



        this.addChild(node);

    },


    removeFromLayer : function(){
        var self = this;
        var winSize = cc.winSize;
        this._node.runAction(new cc.Sequence(
            new cc.MoveTo(0.4, cc.p(winSize.width / 2, -10)).easing(cc.easeBackIn()),
            new cc.CallFunc(function(){
                self.removeFromParent();
            })
        ));
    },





});