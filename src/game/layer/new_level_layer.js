/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/29
 */
game.NewLevelLayer = cc.Layer.extend({
    _node : null,
    _layer : null,
    _action : null,
    ctor:function(level, data, callback){
        this._super();
        var winSize = cc.winSize;
        var self = this;


        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true   ,       // true 为不向下传递
            onTouchBegan: function(touch, event){
                return true;
            },
            onTouchEnded:function(touch, event){
                self.removeFromLayer(callback);
            },
        }, this);


        var node = new cc.Node();
        this._node = node;

        var bg = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('level.png'));
        bg.attr({
            x : bg.width / 2,
            y : bg.height / 2,
        });
        node.addChild(bg);

        node.attr({
            x : winSize.width / 2,
            y : winSize.height / 2,
            width : bg.width,
            height : bg.height,
            anchorX : 0.5,
            anchorY : 0.5,
            //opacity : 1,
        });

        var levelSpr = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('0/' + level + '.png'));
        levelSpr.attr({
            anchorY : 1,
            x : bg.width / 2,
            y : bg.height - 10,
            scale : 3,
        });
        node.addChild(levelSpr);



        this._levelLbl = new cc.LabelTTF('第'+level+'关', 'Arial', 14);
        this._levelLbl.attr({
            anchorX : 0.5,
            anchorY : 0,
            x : bg.width / 2,
            y : 50,
            color : cc.color(0, 234, 255),
        });
        node.addChild(this._levelLbl);

        this._pass_limit_lbl = new cc.LabelTTF('过关条件 图案<='+data.pass_limit+'('+data.bean_count+')', 'Arial', 14);
        this._pass_limit_lbl.attr({
            anchorX : 0.5,
            anchorY : 0,
            x : bg.width / 2,
            y : 20,
            color : cc.color(0, 234, 255),
        });
        node.addChild(this._pass_limit_lbl);




        this.addChild(node);

        this._action = new cc.Sequence(new cc.FadeIn(0.2), new cc.FadeOut(2), new cc.CallFunc(function(){
            self.removeFromLayer(callback);
        }));


    },

    addToLayer : function(layer){
        this._layer = layer;
        layer.addChild(this);
        this._node.runAction(this._action);
    },

    removeFromLayer : function(callback){
        callback();
        this.removeFromParent();
    },


});