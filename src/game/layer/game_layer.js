/**
 * Created by zhaojm on 15/4/1.
 */


game.GameLayer = cc.Layer.extend({

    _bg : null,

    _rect_width : null,

    _level_width : 10,
    _level_height : 10,


    _beans : null,

    _level : null,
    _level_data : null,

    _score : null,
    _count_timer : null,

    _uiLayer : null,


    _status : null,

    _bean_type : null,
    ctor:function () {
        this._super();

        var self = this;
        var winSize = cc.winSize;

        cc.spriteFrameCache.addSpriteFrames(res.gamelayer_plist);

        this._status = game._Enum.status_enum.normal;

        var bg = new cc.Sprite(res.bg_jpg);
        bg.attr({
            anchorX : 0.5,
            anchorY : 0,
            x : winSize.width / 2,
            y : 0,
        });
        this._bg = bg;
        this.addChild(bg);

        this._rect_width = winSize.width / this._level_width;


        this._level = 0;



        this._uiLayer = new game.UILayer();

        this.addChild(this._uiLayer, 2);


        this._level++;
        this.initLevel();
        this._uiLayer.refresh(this._level, this._score, this._count_timer, this._level_data);



        this.addChild(new game.GuideLayer(function(){


            (new game.NewLevelLayer(self._level, self._level_data, function(){

                self.levelStart();

            })).addToLayer(self);


        }));

        this._initTouched();
    },



    addRole : function(role){
        role.addToLayer(this);
    },


    initLevel : function(){

        this._score = 0;
        this._count_timer = 0;
        this._level_data = game._Config.getLevelData(this._level);
        this._bean_type = 1;//[0, 1, 2, 3, 4].getRandomItem();

        var bean_count = this._level_data.bean_count;

        this._beans = [];
        while(bean_count > 0){
            bean_count--;
            var bean = new game.ColorBean(this._bean_type, this._level_data.color_type);
            this._beans.push(bean);
            this.addRole(bean);
        }

        var null_count = this._level_height * this._level_width - this._level_data.bean_count;
        while(null_count > 0){
            null_count--;
            var rand = parseInt(this._beans.length * Math.random());
            this._beans.insert(rand, null);
        }


        this.refreshBeans();


    },

    resumeBeans : function(){

        for (var i = 0, len = this._beans.length; i < len; i++) {
            var iRand = parseInt(len * Math.random());
            var temp = this._beans[i];
            this._beans[i] = this._beans[iRand];
            this._beans[iRand] = temp;
        }

        this.refreshBeans();
    },


    refreshBeans : function(){
        cc.log(this._beans.length);
        for(var i = 0; i < this._beans.length; i++){
            var bean = this._beans[i];
            if(bean){
                var pos = this.getLevelPosByIndex(i);

                bean.setPosition(this.getRealPosByLevelPos(pos));
                bean.setLevelPos(pos.x, pos.y);


            }
        }
    },


    _initTouched : function(){
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true   ,       // true 为不向下传递
            onTouchBegan: function(touch, event){
                //cc.log('touchbegan');
                var pos = touch.getLocation();

                var levelPos = this.getLevelPosByRealPos(pos);

                if(levelPos.x >= 0 && levelPos.x < this._level_width && levelPos.y >= 0 && levelPos.y < this._level_height){
                    this.onTouched(levelPos.x, levelPos.y);
                }

                return true;

            }.bind(this),
        }, this);
    },

    onTouched : function(x, y) {
        cc.log(x, y);
        var touch_bean = this._beans[this.getIndexByLevelPos(cc.p(x, y))];

        if(this._status == game._Enum.status_enum.bomb){
            if(touch_bean){
                //cc.log(touch_bean);

                this._beans[this.getIndexByLevelPos(cc.p(x, y))] = null;
                touch_bean.removeFromLayer(cc.p(x, y));
                this._level_data.bean_count -= 1;
                this._score += 1;
                this._uiLayer.setScore(this._score);
                this._uiLayer.setPassLimit(this._level_data.pass_limit, this._level_data.bean_count);

                this._status = game._Enum.status_enum.normal;
                this._bg.removeAllChildren();

                this.checkLevelOver();

            }
            return;
        }

        if(touch_bean){
            return;
        }



        var left = this.getLeft(x, y);
        var right = this.getRight(x, y);
        var top = this.getTop(x, y);
        var bottom = this.getBottom(x, y);


        if(this._status == game._Enum.status_enum.crose){
            var rounds = [left, right, top, bottom];
            for (var j = 0; j < rounds.length; j++) {
                var bean = rounds[j];
                if(!bean){
                    continue;
                }
                var level_pos = bean._levelPos;
                this._beans[this.getIndexByLevelPos(level_pos)] = null;
                bean.removeFromLayer(cc.p(x, y));
                this._level_data.bean_count -= 1;
                this._uiLayer.setPassLimit(this._level_data.pass_limit, this._level_data.bean_count);
                this._score += 1;
                this._uiLayer.setScore(this._score);
            }
            this._status = game._Enum.status_enum.normal;
            this._bg.removeAllChildren();
        }else if(this._status == game._Enum.status_enum.normal) {

            var sames = this.checkSames(left, right, top, bottom);

            //cc.log(sames);

            if (sames.length == 0) {
                // TODO 没有消除的，要减去 错误惩罚 时间
                this._level_data.time_limit -= 4;
                (new game.SubTimeEffect(this.getRealPosByLevelPos(cc.p(x, y)))).addToLayer(this);
                if (this._level_data.time_limit < 0) {
                    this._level_data.time_limit = 0;
                    this._uiLayer.setTimer(this._level_data.time_limit);
                    this.levelOver();
                    return;
                }
                this._uiLayer.setTimer(this._level_data.time_limit);


            } else {
                // TODO 有要消除的，消除,加分

                (new game.TouchEffect(this.getRealPosByLevelPos(cc.p(x, y)))).addToLayer(this);

                var count = 0;
                for (var i = 0; i < sames.length; i++) {
                    var same = sames[i];

                    for (var j = 0; j < same.length; j++) {
                        var bean = same[j];
                        var level_pos = bean._levelPos;
                        this._beans[this.getIndexByLevelPos(level_pos)] = null;
                        bean.removeFromLayer(cc.p(x, y));

                        count++;
                    }
                }

                this._level_data.bean_count -= count;
                this._uiLayer.setPassLimit(this._level_data.pass_limit, this._level_data.bean_count);

                if(count == 4){

                    this._score += 6;
                    this._uiLayer.setScore(this._score);

                    //TODO 4个，加时间 特效
                    //this._level_data.time_limit += 1.5;
                    //this._uiLayer.setTimer(this._level_data.time_limit);

                }else if(count == 3){

                    this._score += 4;
                    this._uiLayer.setScore(this._score);

                }else{

                    this._score += 2;
                    this._uiLayer.setScore(this._score);
                }



            }
        }

        this.checkLevelOver();


    },

    checkLevelOver:function(){

        if (this._level_data.bean_count <= this._level_data.pass_limit) {
            this.levelOver(true);
        }
    },

    checkSames : function(left, right, top, bottom){

        var sames = [];

        if(left){
            var same = [];
            same.push(left);
            if(right && right._colorType == left._colorType){
                same.push(right);
            }
            if(top && top._colorType == left._colorType){
                same.push(top);
            }
            if(bottom && bottom._colorType == left._colorType){
                same.push(bottom);
            }
            if(same.length > 1){
                sames.push(same);

                if(same.length > 2){
                    return sames;
                }
            }
        }



        if(right){
            var isHave = false;
            for(var i = 0; i < sames.length; i++){
                var same = sames[i];
                isHave = same.indexOf(right) > 0;
                if(isHave){
                    break;
                }
            }
            if(!isHave){
                var same = [];
                same.push(right);

                if(top && top._colorType == right._colorType){
                    same.push(top);
                }
                if(bottom && bottom._colorType == right._colorType){
                    same.push(bottom);
                }
                if(same.length > 1){
                    sames.push(same);

                    if(same.length > 2 || sames.length == 2){
                        return sames;
                    }
                }
            }
        }

        if(top){
            var isHave = false;
            for(var i = 0; i < sames.length; i++){
                var same = sames[i];
                isHave = same.indexOf(top) > 0;
                if(isHave){
                    break;
                }
            }
            if(!isHave){
                var same = [];
                same.push(top);

                if(bottom && bottom._colorType == top._colorType){
                    same.push(bottom);
                }
                if(same.length > 1){
                    sames.push(same);
                }
            }
        }


        return sames;
    },



    getLeft : function(x, y){
        x--;
        if(x < 0){
            return null;
        }else{
            var bean = this._beans[this.getIndexByLevelPos(cc.p(x, y))];
            if(bean){
                return bean;
            }else{
                return this.getLeft(x, y);
            }
        }
    },

    getRight : function(x, y){
        x++;
        if(x >= this._level_width){
            return null;
        }else{
            var bean = this._beans[this.getIndexByLevelPos(cc.p(x, y))];
            if(bean){
                return bean;
            }else{
                return this.getRight(x, y);
            }
        }
    },

    getBottom : function(x, y){
        y--;
        if(y < 0){
            return null;
        }else{
            var bean = this._beans[this.getIndexByLevelPos(cc.p(x, y))];
            if(bean){
                return bean;
            }else{
                return this.getBottom(x, y);
            }
        }
    },

    getTop : function(x, y){
        y++;
        if(y >= this._level_height){
            return null;
        }else{
            var bean = this._beans[this.getIndexByLevelPos(cc.p(x, y))];
            if(bean){
                return bean;
            }else{
                return this.getTop(x, y);
            }
        }
    },

    levelStart : function(){
        this.schedule(this.timer, 1);
    },


    timer : function(){
        var self = this;
        self._count_timer++;
        if(self._count_timer >= self._level_data.time_limit){
            self._count_timer = self._level_data.time_limit;
            self._uiLayer.setTimer(0);
            self.levelOver();
            return;
        }
        self._uiLayer.setTimer(self._level_data.time_limit - self._count_timer);
    },

    levelOver : function(pass){
        //cc.log('level over...is pass->', pass);
        this.unschedule(this.timer);
        if(pass){
            this.addChild(new game.PassLayer(this._level, this._score, this._count_timer, this._level_data), 2);
        }else{
            this.addChild(new game.OverLayer(this._level, this._level_data), 2);
        }
    },

    clearLevel : function(){
        this._beans.forEach(function(item){
            if(item) {
                item.removeFromLayer();
                item = null;
            }
        });
        this._beans = [];

    },

    newLevel : function(){
        cc.log('new level...');
        this._level++;

        this.levelAgain();
    },


    levelAgain : function(){
        var self = this;
        this.clearLevel();
        this.initLevel();
        this._uiLayer.refresh(this._level, this._score, this._count_timer, this._level_data);
        (new game.NewLevelLayer(this._level, this._level_data, function(){

            self.levelStart();

        })).addToLayer(this);

    },

    tips : function(){
        for(var i = 0; i < this._level_width; i++){
            for(var j = 0; j < this._level_height; j++){
                var x = i, y = j;
                var bean = this._beans[this.getIndexByLevelPos(cc.p(x, y))];
                if(!bean){
                    var left = this.getLeft(x, y);
                    var right = this.getRight(x, y);
                    var top = this.getTop(x, y);
                    var bottom = this.getBottom(x, y);

                    var sames = this.checkSames(left, right, top, bottom);

                    if(sames.length > 0){

                        // TODO 显示笑脸，
                        (new game.TipsEffect(this.getRealPosByLevelPos(cc.p(x, y)))).addToLayer(this);

                        return;

                    }

                }

            }
        }
    },

    needBomb : function(){
        //this
        // TODO 没有bean的地方，阴影，显出 bean 的地方
        for(var i = 0; i < this._beans.length; i++){
            var bean = this._beans[i];
            var level_pos = this.getLevelPosByIndex(i);
            var real_pos = this.getRealPosByLevelPos(level_pos);
            var rect = null;
            if(!bean){
                rect = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('pink.png'));
            }else{
                //rect = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('green.png'));
            }
            if(rect){
                rect.attr({
                    x : real_pos.x,
                    y : real_pos.y,
                });
                this._bg.addChild(rect);
            }
        }

    },

    needCrose : function(){
        // TODO 所有的地方，覆盖颜色
        ////var winSize = cc.winSize;
        ////this._bg.setColor(cc.color(219, 250, 199, 255));
        //var green = new cc.Scale9Sprite(cc.spriteFrameCache.getSpriteFrame('green.png'));
        ////this._bg.addChild(green);
        //this.addChild(green);
        //green.attr({
        //    anchorX : 0,
        //    anchorY : 0,
        //    width : this._bg.width,
        //    height : this._bg.height,
        //    x : 0,
        //    y : 0,
        //    color:cc.color(0, 255, 0, 255),
        //});

        for(var i = 0; i < this._beans.length; i++){
            var bean = this._beans[i];
            var level_pos = this.getLevelPosByIndex(i);
            var real_pos = this.getRealPosByLevelPos(level_pos);
            var rect = null;
            if(!bean){
                rect = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('green.png'));
            }else{
                rect = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame('green.png'));
            }
            rect.attr({
                x : real_pos.x,
                y : real_pos.y,
            });
            this._bg.addChild(rect);
        }
    },



    // ----------tools-----------------began............
    getRealPosByLevelPos:function(pos){
        return cc.p(
            pos.x * this._rect_width + this._rect_width / 2,
            pos.y * this._rect_width + this._rect_width / 2
        );
    },

    getLevelPosByRealPos : function(pos){
        var x = parseInt(pos.x / this._rect_width);
        var y = parseInt(pos.y / this._rect_width);
        return cc.p(x, y);
    },


    getIndexByLevelPos : function(p){
        return p.y * this._level_width + p.x;
    },

    getLevelPosByIndex : function(i){
        var x = i % this._level_width;
        var y = Math.floor(i / this._level_width);
        return cc.p(x, y);
    },

    // -----------------tools----------end..............




});