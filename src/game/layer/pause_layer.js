/**
 * Created by zhaojm on 15/4/6.
 */
game.PauseLayer = cc.Layer.extend({

    _node : null,
    ctor:function(){
        this._super();

        var winSize = cc.winSize;
        var self = this;

        //if(game._Config.show_ads && game._Config.language == game._Enum.language.en) {
        //    window['Ads']['fullViewAds']();
        //}


        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true   ,       // true 为不向下传递
            onTouchBegan: function(touch, event){return true;},
        }, this);

        this._node = new cc.Node();

        var bg = new cc.Sprite(res.pause_png);
        bg.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : bg.width / 2,
            y : bg.height / 2,
        });
        this._node.addChild(bg);

        this._node.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : winSize.width / 2,
            y : winSize.height / 2,
            width : bg.width,
            height : bg.height,
        });


        var txtLbl = new cc.LabelTTF('游戏暂停', 'Arial', 18);
        txtLbl.attr({
            anchorX : 0.5,
            anchorY : 1,
            x : bg.width / 2,
            y : bg.height * 0.7,
            color : cc.color(20, 20, 255),
        });
        this._node.addChild(txtLbl);


        var homeFrame  = cc.spriteFrameCache.getSpriteFrame('ui/home.png');
        var homeItem = new cc.MenuItemImage(homeFrame, homeFrame, homeFrame, function(){
            window.location.href="http://mingz.me";
        }, this);

        var startFrame  = cc.spriteFrameCache.getSpriteFrame('ui/start.png');
        var startItem = new cc.MenuItemImage(startFrame, startFrame, startFrame, function(){
            self.removeFromLayer();
            //if(game._Config.show_ads && game._Config.language == game._Enum.language.en) {
            //    window['Ads']['topAds']();
            //}
        }, this);

        var restartFrame  = cc.spriteFrameCache.getSpriteFrame('ui/restart.png');
        var restartItem = new cc.MenuItemImage(restartFrame, restartFrame, restartFrame, function(){
            cc.director.runScene(new game.GameScene());
        }, this);

        var menu = new cc.Menu(homeItem, restartItem, startItem);
        menu.alignItemsHorizontally();
        menu.attr({
            anchorX : 0.5,
            anchorY : 0.5,
            x : bg.width / 2,
            y : bg.height * 0.3,
        });
        this._node.addChild(menu);

        this.addChild(this._node);

    },


    removeFromLayer : function(){
        var self = this;
        var winSize = cc.winSize;
        cc.director.resume();
        this._node.runAction(new cc.Sequence(
            new cc.MoveTo(0.4, cc.p(winSize.width / 2, -10)).easing(cc.easeBackIn()),
            new cc.CallFunc(function(){
                self.removeFromParent();
            })
        ));
    },


});