/**
 * Author: zhaojm
 * Email: <mingzz2013@gmail.com>
 * Create Time: 15/5/30
 */
game.MenuScene = cc.Scene.extend({

    onEnter : function(){
        this._super();

        var self = this;

        this.addChild(new game.MenuLayer());

    },
});